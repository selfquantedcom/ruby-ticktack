require "gosu"

class Board < Gosu::Window

   attr_reader :grid
   attr_reader :square_length
   attr_reader :spacing
   attr_reader :squares_in_row
   attr_reader :player_X_color
   attr_reader :player_Y_color
   attr_accessor :font
   attr_accessor :current_player
   attr_accessor :game_status
   attr_accessor :needs_redraw
   attr_accessor :score_X
   attr_accessor :score_O

   def initialize(squares_in_row, square_length)
      @squares_in_row = squares_in_row
      @square_length = square_length
      @spacing = 10 
      @screen_width = @square_length * squares_in_row + @spacing * (squares_in_row + 1)
      @screen_height = 100 + @square_length * squares_in_row + @spacing * (squares_in_row + 1)

      @current_player = "X"
      @game_status = "ongoing"
      @score_X = 0
      @score_O = 0

      #create grid
      @font = Gosu::Font.new(120)
      @grid_color = Gosu::Color::GRAY
      @player_X_color = Gosu::Color::RED
      @player_O_color = Gosu::Color::BLUE
      @grid = Array.new(squares_in_row) { Array.new(squares_in_row, nil) }      
      
      #Populate the grid with Fields
      new_grid(self)      

      #visualize grid
      @needs_redraw = true
      super(@screen_width, @screen_height)
   end
   
   def new_grid(board)      
      for x in (0...board.squares_in_row)                 
         for y in (0...board.squares_in_row)
            board.grid[x][y] = Field.new(board, x,y,nil)
         end         
      end
      return board
   end

   def draw()      
      for x in (0...@squares_in_row)                 
         for y in (0...@squares_in_row)

            #draw board and player moves
            cord_X = @grid[x][y].top_left_corner[0]
            cord_Y = @grid[x][y].top_left_corner[1]
            draw_rect(cord_X, cord_Y, @square_length, @square_length, @grid_color, z = 0, mode = :default)  

            player = @grid[x][y].player
            if player == "X"
               @font.draw('X', cord_X + @square_length / 6, cord_Y - square_length / 10, 2)
            elsif player == "O"        
               @font.draw('O', cord_X + @square_length / 10, cord_Y - square_length / 10, 2)
            end
         end   
      end

      announcment = Gosu::Font.new(20)
      #draw score
      announcment.draw("X: " + @score_X.to_s,  @spacing + @square_length / 2, @screen_height - 100, 3)      
      announcment.draw("O: " + @score_O.to_s,  @screen_width - @spacing - @square_length / 2, @screen_height - 100, 3)      

      #game status i.e. winning player
      if @game_status != "ongoing"
         if @game_status == "Draw"
            message = "It is a draw"
         else
            message = "Player " + @game_status + " wins"
         end
         announcment.draw(message,  @screen_width / 2 - @square_length / 2, @screen_height - 100, 3)      
         announcment.draw("To restart press 'Space'. To reset score press 'R'",  @screen_width / 5, @screen_height - 70, 3)      
      end

      @needs_redraw = false
   end

   #handle user input
   def button_down(id)
      if (id == Gosu::MsLeft)
         for x in (0...@squares_in_row)                 
            for y in (0...@squares_in_row)
               pos_X = @grid[x][y].center[0]
               pos_Y = @grid[x][y].center[1]
               if (Gosu.distance(mouse_x, mouse_y, pos_X, pos_Y) < @square_length / 2)
                  pos_X = @grid[x][y].row_pos
                  pos_Y = @grid[x][y].col_pos
                  make_move(self, pos_X, pos_Y, @current_player, true)
               end         
            end   
         end  
      end

      if (id == Gosu::KbSpace)        
         reset_game()      
         @needs_redraw = true
      end

      if (id == Gosu::KbR)        
         reset_game()
         @score_X = 0
         @score_O = 0      
         @needs_redraw = true
      end
   end

   #update game state changes 
   def make_move(board, pos_X, pos_Y, player, redraw)  
      if is_empty(board.grid[pos_X][pos_Y])
         if board.game_status == "ongoing"
            board.grid[pos_X][pos_Y].player = player 
            switch_player(board)                 
            board.game_status = game_state(board)              

            if board.game_status != "ongoing"
               if board.game_status == "X"
                  board.score_X += 1
               elsif board.game_status == "O"
                  board.score_O += 1
               end
            end

            board.needs_redraw = redraw            
         end
      end
   end

   #determine game status i.e. win, loss, draw, ongoing
   def game_state(board)

      row_count_X = 0
      row_count_O = 0
      col_count_X = 0
      col_count_O = 0
      diagonal_1_X = 0
      diagonal_1_O = 0
      diagonal_2_X = 0
      diagonal_2_O = 0
      
      for x in (0...board.squares_in_row)                 
         for y in (0...board.squares_in_row)

            #check rows
            if board.grid[x][y].player == "X"
               row_count_X += 1
            elsif board.grid[x][y].player == "O"
               row_count_O += 1
            else 
               row_count_O = 0 
               row_count_X = 0
            end

            #check columns
            if board.grid[y][x].player == "X"
               col_count_X += 1
            elsif board.grid[y][x].player == "O"
               col_count_O += 1
            else 
               col_count_O = 0 
               col_count_X = 0
            end

            #check diagonal_1
            if x == y
               if board.grid[y][x].player == "X"
                  diagonal_1_X += 1
               elsif board.grid[y][x].player == "O"
                  diagonal_1_O += 1
               else 
                  diagonal_1_X = 0                   
                  diagonal_1_O = 0                   
               end
            end

            #check diagonal_2
            if x == squares_in_row - 1 - y
               if board.grid[y][x].player == "X"
                  diagonal_2_X += 1
               elsif board.grid[y][x].player == "O"
                  diagonal_2_O += 1
               else 
                  diagonal_2_X = 0                   
                  diagonal_2_O = 0                   
               end
            end
         end 

         #determine winner for rows
         if row_count_X == squares_in_row            
            return "X"
         elsif row_count_O == squares_in_row            
            return "O"
         else
            row_count_O = 0
            row_count_X = 0               
         end

         #determine winner for columns
         if col_count_X == squares_in_row            
            return "X"
         elsif col_count_O == squares_in_row            
            return "O"
         else
            col_count_O = 0
            col_count_X = 0               
         end
      end 

      #determine winner for diagonals
      if diagonal_1_X == squares_in_row         
         return "X"
      elsif diagonal_1_O == squares_in_row         
         return "O"
      elsif diagonal_2_X == squares_in_row         
         return "X"
      elsif diagonal_2_O == squares_in_row         
         return "O"
      else
         diagonal_1_X = 0
         diagonal_2_X = 0                              
         diagonal_1_O = 0
         diagonal_2_O = 0               
      end

      #check for draw
      if board_full(board)      
         return "Draw"
      else         
         return "ongoing"
      end
   end

   #board full
   def board_full(board)
      board_full = true
      for x in (0...squares_in_row)                 
         for y in (0...squares_in_row)
            board.grid[x][y].player == nil
            board_full = false
         end         
      end 
      return board_full
   end

   def switch_player(board)
      if board.current_player == "X"
         board.current_player = "O"
      else
         board.current_player = "X"
      end
   end

   #override inherited method to prevent constant board redrawing when not necessary
   def needs_redraw(board)
      return board.needs_redraw
   end

   #display cursor in GUI
   def needs_cursor?
      return true
   end

   def reset_game()
      @game_status = "ongoing"
      #reset grid
      @grid = Array.new(squares_in_row) { Array.new(squares_in_row, nil) }      

      #Populate the grid with Fields
      for x in (0...squares_in_row)                 
         for y in (0...squares_in_row)
            @grid[x][y] = Field.new(self, x,y,nil)
         end         
      end
   end

   def is_empty(field)
      if field.player == nil
         return true
      else
         return false
      end      
   end

   ###Section for machine player###
   SIMULATION_ROUNDS = 50
   PENALTY = -1000
   
   def calculate_best_move(provided_board)
      board = provided_board.clone
      board_score = board.clone

      #Distinguish occupied places on score board
      for x in (0...board_score.squares_in_row)                 
         for y in (0...board_score.squares_in_row)
            if(board_score.grid[x][y].player != nil)
               board_score.grid[x][y].player = PENALTY
            else
               board_score.grid[x][y].player = 0
            end
         end
      end

      #run simulation
      while(game_state(board)=="ongoing")
         row_move = rand(0...board.squares_in_row)
         col_move = rand(0...board.squares_in_row)

         make_move(board, row_move, col_move, board.current_player, false) 
         switch_player(board)
      end

      winning_player = game_state(board)
      for x in (0...board.squares_in_row)                 
         for y in (0...board.squares_in_row)
            if(board.grid[x][y].player == winning_player)
               board_score.grid[x][y].player += 1
            else
               board_score.grid[x][y].player -= 1
            end            
         end
      end
      p board_score


   end



end #end of Board class

#Fields for individual positions on a board 
class Field 

   attr_accessor :player
   attr_reader :row_pos
   attr_reader :col_pos
   attr_reader :top_left_corner
   attr_reader :center

   def initialize(sender, row_pos, col_pos, player)
      @player = player
      @row_pos = row_pos
      @col_pos = col_pos
      @center = [sender.spacing * (1+row_pos) + sender.square_length*row_pos + sender.square_length / 2, sender.spacing * (1+col_pos) + sender.square_length*col_pos + sender.square_length / 2] 
      @top_left_corner = [sender.spacing * (1+row_pos) + sender.square_length*row_pos, sender.spacing * (1+col_pos) + sender.square_length*col_pos]
   end



end #end of Field class

#run game here
board = Board.new(5, 100)

#THIS NOTATION IS WEIRD. IS THERE A STATIC METHOD? 
board.calculate_best_move(board)
#board.show














